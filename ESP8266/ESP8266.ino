#include <SoftwareSerial.h>

#define LED 7
#define SENSOR A5

SoftwareSerial logsSerial(2,3);

const String networkName = "MyNetworkName";
const String networkPassword = "MyNetworkPassword";
const String serverIP = "192.168.0.100";
const String serverPort = "1337";
const int deviceConnectionID = 0;
const int bufferFillDelay = 1000;

int sensorTriggerValue = 200;
    
void setup() {
  Serial.begin(9600);
  logsSerial.begin(9600);
  
  pinMode(LED, OUTPUT);
  pinMode(SENSOR, INPUT);
  
  connectToNetwork();
  connectToServer();
  
  digitalWrite(LED, HIGH);
  delay(200);
  digitalWrite(LED, LOW);
  delay(200);
  digitalWrite(LED, HIGH);
  delay(200);
  digitalWrite(LED, LOW);
}
     
void loop() {
  int sensorMeasure = analogRead(SENSOR);
      
  if (sensorMeasure < sensorTriggerValue) {
    sendMessageToServer("Sensor measured value: " + String(sensorMeasure));
  }
  
  receiveMessageFromServer();
}

void receiveMessageFromServer() {
  if (Serial.available()) {
    if (Serial.find("+IPD,")) {
      delay(bufferFillDelay);
      
      String response = "";
      int serverConnectionID = Serial.read() - 48;
          
      if (Serial.find("\"activateLED\":true")) {
        digitalWrite(LED, HIGH);
        response += "Switched LED on pin: " + String(LED) + " to HIGH";
      }
      
      if (Serial.find("\"activateLED\":false")) {
        digitalWrite(LED, LOW);
        response += "Switched LED on pin: " + String(LED) + " to LOW";
      }
      
      if (Serial.find("\"closeConnection\":true")) {
        sendATCommand("AT+CIPCLOSE=" + String(deviceConnectionID), 1000);
        
        response += "Connection closed!";
      }
      
      if (Serial.find("\"newSensorTriggerValue\":")) {
        int newSensorTriggerValue = ((Serial.read() - 48) * 1000) + ((Serial.read() - 48) * 100) + ((Serial.read() - 48) * 10) + (Serial.read() - 48);
        sensorTriggerValue = newSensorTriggerValue;
        
        response += "Sensor trigger value set to: " + String(newSensorTriggerValue);
      }
        
      sendATCommand("AT+CIPSEND=" + String(serverConnectionID) + "," + String(response.length()), 100);
      
      Serial.print(response + "\r\n");
      logsSerial.println("receiveMessageFromServer connection id: " + String(serverConnectionID) + ", response: " + response);
      
      sendATCommand("AT+CIPCLOSE=" + String(serverConnectionID), 1000);
    }
  }
}

void sendMessageToServer(String message) {
  sendATCommand("AT+CIPSEND=" + String(deviceConnectionID) + "," + String(message.length()), 100);
  
  Serial.print(message + "\r\n");
  logsSerial.println("sendMessageToServer message: " + message);
}

void sendATCommand(String command, const int timeout) {
  Serial.print(command + "\r\n");
  
  String response = "";
  long int time = millis();
  
  while ((time + timeout) > millis()) {
    while (Serial.available()) {
      response += (char)Serial.read();
    }
  }
  
  logsSerial.println("sendATCommand response: " + response);
}

void connectToNetwork() {
  //sendATCommand("AT+RST", 2000);
  sendATCommand("AT+CWMODE=1", 1000);
  sendATCommand("AT+CWJAP=\"" + networkName + "\",\"" + networkPassword + "\"", 3000);
  sendATCommand("AT+CIFSR", 2000);
  sendATCommand("AT+CIPMODE=0", 1000);
  sendATCommand("AT+CIPMUX=1", 1000);
  sendATCommand("AT+CIPSERVER=1,80", 1000);
  
  logsSerial.println("Connected to the network");
}

void connectToServer() {
  sendATCommand("AT+CIPSTART=" + String(deviceConnectionID) + ",\"TCP\",\"" + serverIP + "\"," + serverPort, 1000);
  
  logsSerial.println("Connected to the server");
}
