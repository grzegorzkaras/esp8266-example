const net = require("net");

const port = 1337;
const clients = [];

const settings = {
  activateLED: true,
  closeConnection: false,
  newSensorTriggerValue: 1023
};

const server = net.createServer(socket => {
  socket.name = `${socket.remoteAddress}:${socket.remotePort}`;

  clients.push(socket);
  
  console.log(`Client connected: ${socket.name}`);

  socket.on("data", data => console.log(`Data received: ${data}`));

  socket.on("end", () => {
    clients.splice(clients.indexOf(socket), 1);
    console.log(`Client ${socket.name} left`);
  });
  
  const message = JSON.stringify(settings);
  socket.write(message);
  
  socket.pipe(socket);
});

server.on("error", err => console.log(`Error: ${err}`));

server.listen(port, () => console.log(`Server running at port ${port}`));